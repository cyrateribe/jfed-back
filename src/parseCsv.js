const csv = require('fast-csv');

exports.parseCsv = (file, cb) => {
    let parsedData = [];
    csv.fromPath(file, {headers: true, delimiter: ';'})
        .on('data-invalid', () => {
            return cb('Nie poprawny plik');
        })
        .on('data', (data) => {
            parsedData.push(data);
        })
        .on('end', () => {
            return cb(parsedData);
        });
};
