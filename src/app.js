const express = require('express');
const fileUpload = require('express-fileupload');

const config = require('./config.js');

const CSV = require('./parseCsv');

const fs = require('fs');

const app = express();

let parsedData;

var configJson = JSON.parse(fs.readFileSync(`${__dirname}/../uploaded/config.json`, 'utf8'));


String.prototype.shuffle = function () {
    var a = this.split(""),
        n = a.length;

    for (var i = n - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
    return a.join("");
};

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(fileUpload());

app.get('/', (req, res) => {
    res.send('Pong!');
});

app.get('/diff', (req, res) => {
    const fileExt = req.query.fileName.split('.').pop();
    if (fileExt !== 'csv') res.status(500).send('Nieprawidwoły plik');
    CSV.parseCsv(`${__dirname}/../uploaded/${req.query.fileName}`, (data) => {
        parsedData = data;
        let diff = [];
        parsedData.map((el) => {
            if (el.Platform.length > 0 && el.Slot.length > 0) {
                for (let service in configJson) {
                    configJson[service].units.map((platform) => {
                        if (platform.platform === el.Platform && platform.config.slot == Number(el.Slot)) {
                            const parsedValue = parseFloat(el.Value.replace(/,/g, '.'));
                            const parsedMinimum = parseFloat(el.Minimum.replace(/,/g, '.'));

                            if (parsedValue && parsedValue !== platform.config.measures.value) {
                                platform.config.measures.value = parsedValue;
                                diff.push(`Zmiana w platformie ${el.Platform} slot ${el.Slot} w polu Value: z ${platform.config.measures.value} na ${parsedValue}`);
                            }
                            if (parsedMinimum && parsedMinimum !== platform.config.measures.minimum) {
                                platform.config.measures.minimum = parsedMinimum;
                                diff.push(`Zmiana w platformie ${el.Platform} slot ${el.Slot} w polu Minimum: z ${platform.config.measures.minimum} na ${parsedMinimum}`);
                            }
                        }
                    });
                }
            }
        });

        fs.writeFile(`${__dirname}/../uploaded/config.json`, JSON.stringify(configJson, null, 2));
        res.send(diff);
    });

});


app.post('/uploadFile', (req, res) => {
    if (!req.files)
        return res.status(400).send('Brak pliku');

    const newFileName = new Date().valueOf() + config.string.shuffle() + '.' + req.files.file.name.split('.').pop();
    let uploadedFile = req.files.file;

    uploadedFile.mv(`${__dirname}/../uploaded/${newFileName}`, function (err) {
        if (err)
            return res.status(500).send(err);

        res.json({fileName: `${newFileName}`})
    });
});

app.listen(config.server.port);

console.log(`Server listening on port : ${config.server.port}`);

module.exports = app;

